package main

import (
 "testing"
)

func TestGetMessage(t *testing.T) {
	message := getMessage()
	if message == "World" {
		t.Log("the result is ok")
	} else {
		t.Fatal("the result is wrong")
	}
}

func TestAdd1(t *testing.T) {
	sum := Add(1,2)
	if sum == 3 {
		t.Log("the result is ok")
	} else {
		t.Fatal("the result is wrong")
	}
}

func TestAdd2(t *testing.T) {
	sum := Add(3,4)
	if sum == 34 {
		t.Log("the result is ok")
	} else {
		t.Fatal("the result is wrong")
	}
}


func TestSub1(t *testing.T) {
	sum := Sub(2,1)
	if sum == 1 {
		t.Log("the result is ok")
	} else {
		t.Fatal("the result is wrong")
	}
}

func TestSub2(t *testing.T) {
	sum := Sub(4,1)
	if sum == 1 {
		t.Log("the result is ok")
	} else {
		t.Fatal("the result is wrong")
	}
}