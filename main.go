package main

import (
	"fmt"
	"log"
	"net/http"
)

var (
	address string
)

func init() {
	//address = "0.0.0.0:8080"
}

func main() {
    var a string;
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello %s\n", getMessage())
	})
	log.Printf("a is ... %s", a)
	log.Printf("listening on %s...", address)
	log.Fatal(http.ListenAndServe(address, nil))
}

func getMessage() string {
	return "World"
}

func Add(a,b int) int{
    log.Printf("a+b is ... %d", a+b)
	return a+b
}
func Sub(a,b int) int{
    log.Printf("a-b is ... %d", a-b)
	return a-b
}
func Multi(a,b int) int{
    log.Printf("a*b is ... %d", a*b)
	return a*b
}
func Chu(a,b int) int{
    log.Printf("a/b is ... %d", a/b)
	return a/b
}




