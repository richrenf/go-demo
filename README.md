### 简介
<hr>
这是一个简单的golang web服务示例，不依赖任何第三方框架

### 文件解释
<hr>
样例包括:

README.md   -   当前文件

.gitignore  -   git忽略提交

Dockerfile  -   部署镜像的dockerfile(镜像部署才有此文件)

main.go -   主程序

deploy.sh   -   ECS部署脚本(主机部署才有此文件)

Makefile    -   编译脚本

main_test.go    -   单元测试例子


### 快速开始
<hr>
下述内容将引导用户在自己的电脑上构建部署此项目

1.构建：

```
make build
```

2.部署

```
make run

```
  
本仓库于 2019-08-22 15:13:58 使用了源码自动生成模板 golang-normal-web 。详情见template_info.md文件。
