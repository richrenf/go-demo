# 源码自动生成模板 golang-normal-web

### 概述

* 模板: golang-normal-web
* 模板使用时间: 2019-08-22 15:13:58

### Docker
* Image: registry.cn-beijing.aliyuncs.com/code-template/golang-normal-web
* Tag: 20190628
* SHA256: 9a0de28e2d9dd7ef70413afa423b8ac72b8d32597af21a9e053bb1f5387c59a7

### 用户输入参数
* repoUrl: "git@code.aliyun.com:77252-rdc/go-test.git" 
* needDockerfile: "N" 
* appName: "application" 
* from: "newPipeline" 
* codeRepo: "code.aliyun.com/77252-rdc/go-test" 
* operator: "aliyun_038252" 

### 上下文参数
* appName: application
* operator: aliyun_038252
* gitUrl: git@code.aliyun.com:77252-rdc/go-test.git
* branch: master


### 命令行
	sudo docker run --rm -v `pwd`:/workspace -e repoUrl="git@code.aliyun.com:77252-rdc/go-test.git" -e needDockerfile="N" -e appName="application" -e from="newPipeline" -e codeRepo="code.aliyun.com/77252-rdc/go-test" -e operator="aliyun_038252"  registry.cn-beijing.aliyuncs.com/code-template/golang-normal-web:20190628

