.PHONY: build
build:
	go build -o target/main main.go

.PHONY: run
run:
	cd target && ./main